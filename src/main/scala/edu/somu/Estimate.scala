package edu.somu

import org.apache.spark.SparkConf
import org.apache.spark.sql._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.sql.execution.columnar.DefaultCachedBatch

object Estimate {

  def getDataframeSize[T](dataframe: Dataset[T]): Double = {

    val sampleSize = 0.1;

    val sampledDf = dataframe.sample(sampleSize)

    sampledDf.persist(StorageLevel.MEMORY_AND_DISK_SER).count()

    val sizeInBytes = sampledDf.sparkSession.sessionState.executePlan(sampledDf.queryExecution.logical)
      .optimizedPlan.stats.sizeInBytes

    sampledDf.unpersist()

    val result = (sizeInBytes.longValue()./(sampleSize)).doubleValue

    result / 1024

  }

  def getConfig = {

    val conf = new SparkConf()

    conf.set("setWarnUnregisteredClasses","true")
    conf.registerKryoClasses(
      Array(
        classOf[scala.collection.mutable.WrappedArray.ofRef[_]],
        classOf[org.apache.spark.sql.types.StructType],
        classOf[Array[org.apache.spark.sql.types.StructType]],
        classOf[org.apache.spark.sql.types.StructField],
        classOf[Array[org.apache.spark.sql.types.StructField]],
        Class.forName("org.apache.spark.sql.types.StringType$"),
        Class.forName("org.apache.spark.sql.types.LongType$"),
        Class.forName("org.apache.spark.sql.types.BooleanType$"),
        Class.forName("org.apache.spark.sql.types.DoubleType$"),
        Class.forName("[[B"),
        classOf[org.apache.spark.sql.types.Metadata],
        classOf[org.apache.spark.sql.types.ArrayType],
        Class.forName("org.apache.spark.sql.execution.joins.UnsafeHashedRelation"),
        classOf[org.apache.spark.sql.catalyst.InternalRow],
        classOf[Array[org.apache.spark.sql.catalyst.InternalRow]],
        classOf[org.apache.spark.sql.catalyst.expressions.UnsafeRow],
        Class.forName("org.apache.spark.sql.execution.joins.LongHashedRelation"),
        Class.forName("org.apache.spark.sql.execution.joins.LongToUnsafeRowMap"),
        classOf[org.apache.spark.util.collection.BitSet],
        classOf[org.apache.spark.sql.types.DataType],
        classOf[Array[org.apache.spark.sql.types.DataType]],
        Class.forName("org.apache.spark.sql.types.NullType$"),
        Class.forName("org.apache.spark.sql.types.IntegerType$"),
        Class.forName("org.apache.spark.sql.types.TimestampType$"),
        Class.forName("org.apache.spark.sql.execution.columnar.DefaultCachedBatch"),
        Class.forName("org.apache.spark.sql.catalyst.expressions.GenericInternalRow"),
        Class.forName("org.apache.spark.unsafe.types.UTF8String"),
//        Class.forName("org.apache.spark.sql.execution.datasources.FileFormatWriter$WriteTaskResult"),
//        Class.forName("org.apache.spark.internal.io.FileCommitProtocol$TaskCommitMessage"),
        Class.forName("scala.collection.immutable.Set$EmptySet$"),
//        Class.forName("scala.reflect.ClassTag$$anon$1"),
        Class.forName("java.lang.Class")
      )
    )
  }

    def main(args: Array[String]): Unit = {
      val spark = SparkSession.builder
        .master("local[*]")
        .appName("Size Application")
//        .config(getConfig)
        .config("spark.driver.host", "localhost")
        .config("spark.driver.bindAddress", "localhost")
//        .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
//        .config("spark.kryoserializer.buffer", "1024k")
//        .config("spark.kryoserializer.buffer.max", "1024m")
//        .config("spark.kryo.registrationRequired", "true")
        .getOrCreate()

      val dataFrame = spark.read.parquet("/Users/somsing/Desktop/SampleData/compressedStocks")

      println(getDataframeSize(dataFrame))

      Thread.sleep(60000)

    }

}
