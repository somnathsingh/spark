package edu.somu.plugin

import org.apache.spark.api.plugin.SparkPlugin

class CustomPlugin extends SparkPlugin {

  override def driverPlugin(): CDriverPlugin = new CDriverPlugin()

  override def executorPlugin(): CExecutorPlugin = new CExecutorPlugin()
  
}
