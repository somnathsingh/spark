package com.tutorial.spark;

public class Test {

    public static void main(String[] args) {

        String value = "Hap$y";
        String ch = "\u0009";
        String bk = "\u000b";
        String del = ".\\t@&\\n.";

        String replace = del.replace("\\t", ch).replace("\\n", bk);

        System.out.println(replace);

    }
}
